﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenT1
{
    public class Jugador
    {
        public List<Carta> Cartas { get; set; }
        public bool Ganador { get; set; }
        public double Puntos { get; set; }
    }
}
