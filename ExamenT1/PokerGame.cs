﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenT1
{
    public class PokerGame
    {
        public Jugador EscaleraReal(Jugador jugador1, Jugador jugador2)
        {
            if (jugador1.Cartas.Count < 5 || jugador2.Cartas.Count < 5)
            {
                throw new Exception("Se necesitan 5 cartas");
            }

            if (jugador1.Cartas.Count == 5 && jugador2.Cartas.Count == 5)
            {
                CalculaEscalera(jugador1);
                CalculaEscalera(jugador2);

                return Ganador(jugador1, jugador2);
            }
            throw new Exception("No cumple con lo solicitado");

        }

        void CalculaEscalera(Jugador jugador)
        {
            var palo = jugador.Cartas[0].palo;
            var suma = 0;
            foreach (var carta in jugador.Cartas)
            {
                if (carta.palo == palo)
                {
                    suma += carta.numero;
                }
            }

            if (suma == 60)
            {
                jugador.Puntos = 649.739;
            }
            else
            {
                return;
            }
        }

        public Jugador Ganador(Jugador jugador, Jugador jugador1)
        {

            if (jugador.Puntos > jugador1.Puntos)
            {
                return jugador;
            }

            return jugador1;
        }

        public bool JugadoresValidos(List<Jugador> jugadores)
        {
            if (jugadores.Count > 1 && jugadores.Count < 6)
            {
                return true;
            }

            throw new Exception("No cumple con lo solicitado");
        }

        public bool CartasValidas(List<Carta> cartas)
        {
            for (int i = 0; i < cartas.Count; i++)
            {
                for (int j = 0; j < cartas.Count; j++)
                {
                    if (i != j)
                    {
                        if (cartas[i].numero == cartas[j].numero && cartas[i].palo == cartas[j].palo)
                        {
                            throw new Exception("Cartas iguales en la baraja");
                        }
                    }
                }
            }
            return true;
        }

        public Jugador Color(Jugador jugador1, Jugador jugador2)
        {

            if (jugador1.Cartas.Count < 5 || jugador2.Cartas.Count < 5)
            {
                throw new Exception("Se necesitan 5 cartas");
            }

            if (jugador1.Cartas.Count == 5 && jugador2.Cartas.Count == 5)
            {
                CalculaColor(jugador1);
                CalculaColor(jugador2);

                return Ganador(jugador1, jugador2);
            }
            throw new Exception("No cumple con lo solicitado");
        }

        void CalculaColor(Jugador jugador)
        {
            var palo = jugador.Cartas[0].palo;
            var suma = 0;
            foreach (var carta in jugador.Cartas)
            {
                if (carta.palo == palo)
                {
                    suma += 1;
                }
            }

            if (suma == 5)
            {
                jugador.Puntos = 507;
            }
            else
            {
                return;
            }
        }

        public Jugador DoblePareja(Jugador jugador1, Jugador jugador2)
        {

            if (jugador1.Cartas.Count < 5 || jugador2.Cartas.Count < 5)
            {
                throw new Exception("Se necesitan 5 cartas");
            }

            if (jugador1.Cartas.Count == 5 && jugador2.Cartas.Count == 5)
            {
                CalculaDoblePareja(jugador1);
                CalculaDoblePareja(jugador2);

                return Ganador(jugador1, jugador2);
            }
            throw new Exception("No cumple con lo solicitado");
        }

        public void CalculaDoblePareja(Jugador jugador)
        {
            var iguales1 = 0;
            var iguales2 = 0;
            var numero1 = jugador.Cartas[0].numero;
            var numero2 = 0;

            foreach (var carta in jugador.Cartas)
            {
                if (carta.numero != numero1)
                {
                    numero2 = carta.numero;
                }
            }
            foreach (var carta in jugador.Cartas)
            {
                if (carta.numero == numero1)
                {
                    iguales1++;
                }
                if (carta.numero == numero2)
                {
                    iguales2++;
                }
            }

            if (iguales1 == 2 && iguales2 == 2)
            {
                jugador.Puntos = 20;
            }
            else
            {
                return;
            }
        }
    }
}

