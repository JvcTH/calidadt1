using ExamenT1;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TestT1
{
    public class Tests
    {
        Jugador jugador1;
        Jugador jugador2;

        [SetUp]
        public void Setup()
        {
            jugador2 = new Jugador()
            {
                Puntos = 0
            };

            jugador1 = new Jugador()
            {
                Puntos = 0
            };
        }

        [Test]
        public void CantidadJugadoresValida()
        {
            // cartas 4
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var jugador3 = new Jugador();
            var jugador4 = new Jugador();
            var jugador5 = new Jugador();
            jugadores.Add(jugador5);
            jugadores.Add(jugador4);
            jugadores.Add(jugador3);
            var poker = new PokerGame();
            Assert.AreEqual(true, poker.JugadoresValidos(jugadores));
        }

        [Test]
        public void CantidadJugadoresInvalida()
        {
            // cartas 4
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.JugadoresValidos(jugadores));
        }

        [Test]
        public void CantidadJugadoresInvalida2()
        {
            // cartas 4
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(jugador1);
            jugadores.Add(jugador2);
            var jugador3 = new Jugador();
            var jugador4 = new Jugador();
            var jugador5 = new Jugador();
            var jugador6 = new Jugador();
            jugadores.Add(jugador5);
            jugadores.Add(jugador4);
            jugadores.Add(jugador3);
            jugadores.Add(jugador6);
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.JugadoresValidos(jugadores));
        }

        [Test]
        public void CartasValidas()
        {
            // cartas 4
            List<Carta> cartas = new List<Carta>() {
                new Carta {numero = 1, palo = "Corazon" },
                new Carta {numero = 2, palo = "Trebol" },
                new Carta {numero = 3, palo = "Diamante" },
                new Carta {numero = 4, palo = "Espada" },
                new Carta {numero = 5, palo = "Corazon" },
                new Carta {numero = 6, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };
            var poker = new PokerGame();
            Assert.AreEqual(true, poker.CartasValidas(cartas));
        }

        [Test]
        public void CartasInvalidas()
        {
            List<Carta> cartas = new List<Carta>() {
                new Carta {numero = 1, palo = "Corazon" },
                new Carta {numero = 2, palo = "Trebol" },
                new Carta {numero = 3, palo = "Diamante" },
                new Carta {numero = 4, palo = "Espada" },
                new Carta {numero = 5, palo = "Corazon" },
                new Carta {numero = 6, palo = "Trebol" },
                new Carta {numero = 6, palo = "Trebol" },
            };
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.CartasValidas(cartas));
        }

        [Test]
        public void EscaleraRealGanaJugador1()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 14, palo = "Trebol" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;

            var poker = new PokerGame();
            var ganador = poker.EscaleraReal(jugador1, jugador2);
            Assert.AreEqual(jugador1, ganador);
        }

        [Test]
        public void EscaleraRealGanaJugador2()
        {
            // No tienen el mismo palo
            var lista = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
            };
            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 14, palo = "Trebol" },
            };
            jugador1.Cartas = lista;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            var ganador = poker.EscaleraReal(jugador1, jugador2);
            Assert.AreEqual(jugador2, ganador);
        }

        [Test]
        public void EscaleraRealExepcion1()
        {
            // cartas 1
            var lista = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
            };
            jugador1.Cartas = lista;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.EscaleraReal(jugador1, jugador2));
        }

        [Test]
        public void EscaleraRealExepcion2()
        {
            // cartas 2
            var lista = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 10, palo = "Trebol" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 10, palo = "Trebol" },
            };
            jugador1.Cartas = lista;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.EscaleraReal(jugador1, jugador2));
        }

        [Test]
        public void EscaleraRealExepcion3()
        {
            // cartas 3
            var lista = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 14, palo = "Trebol" },

            };
            jugador1.Cartas = lista;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.EscaleraReal(jugador1, jugador2));
        }

        [Test]
        public void EscaleraRealExepcion4()
        {
            // cartas 4
            var lista = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 14, palo = "Trebol" },

            };
            jugador1.Cartas = lista;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.EscaleraReal(jugador1, jugador2));
        }


        [Test]
        public void ColorExitosoJugador2()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Espada" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;

            var poker = new PokerGame();
            var ganador = poker.Color(jugador1, jugador2);
            Assert.AreEqual(jugador2, ganador);
        }


        [Test]
        public void ColorExitosoJugador1()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Espada" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista2;
            jugador2.Cartas = lista1;

            var poker = new PokerGame();
            var ganador = poker.Color(jugador1, jugador2);
            Assert.AreEqual(jugador1, ganador);
        }


        [Test]
        public void ColorExitosoInvalido1()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista2;
            jugador2.Cartas = lista1;

            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.Color(jugador1, jugador2));
        }

        [Test]
        public void ColorExitosoInvalido2()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista2;
            jugador2.Cartas = lista1;

            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.Color(jugador1, jugador2));
        }


        [Test]
        public void ColorExitosoInvalido3()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 13, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista2;
            jugador2.Cartas = lista1;

            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.Color(jugador1, jugador2));
        }

        [Test]
        public void ColorExitosoInvalido4()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
            };
            jugador1.Cartas = lista2;
            jugador2.Cartas = lista1;

            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.Color(jugador1, jugador2));
        }

        [Test]
        public void DobleParejGanaJugador1()
        {
            var lista1 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Corazon" },
                new Carta {numero = 12, palo = "espadas" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
                new Carta {numero = 14, palo = "Corazon" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            var ganador = poker.DoblePareja(jugador1, jugador2);
            Assert.AreEqual(jugador1, ganador);
        }

        [Test]
        public void DobleParejGanaJugador2()
        {
            var lista1 = new List<Carta>()
            {
                 new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 1, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 14, palo = "Trebol" },
                new Carta {numero = 12, palo = "Corazon" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
                new Carta {numero = 14, palo = "Corazon" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            var ganador = poker.DoblePareja(jugador1, jugador2);
            Assert.AreEqual(jugador2, ganador);
        }

        [Test]
        public void DobleParejaInvalido()
        {
            var lista1 = new List<Carta>()
            {
                 new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 14, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
                new Carta {numero = 14, palo = "Corazon" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.DoblePareja(jugador1, jugador2));
        }

        [Test]
        public void DobleParejaInvalido1()
        {
            var lista1 = new List<Carta>()
            {
                 new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 14, palo = "Trebol" },
                new Carta {numero = 12, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
                new Carta {numero = 14, palo = "Corazon" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.DoblePareja(jugador1, jugador2));
        }

        [Test]
        public void DobleParejaInvalido2()
        {
            var lista1 = new List<Carta>()
            {
                 new Carta {numero = 10, palo = "Trebol" },
                new Carta {numero = 11, palo = "Trebol" },
                new Carta {numero = 7, palo = "Diamante" },
            };

            var lista2 = new List<Carta>()
            {
                new Carta {numero = 14, palo = "Trebol" },
                new Carta {numero = 14, palo = "Diamante" },
                new Carta {numero = 14, palo = "Corazon" },
            };
            jugador1.Cartas = lista1;
            jugador2.Cartas = lista2;
            var poker = new PokerGame();
            Assert.Throws(typeof(Exception), () => poker.DoblePareja(jugador1, jugador2));
        }

    }
}